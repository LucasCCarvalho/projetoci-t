//
//  LoginContentModel.swift
//  Architecture
//
//  Created by Lucas Carvalho on 10/06/19.
//  Copyright © 2019 Lucas Carvalho. All rights reserved.
//

import Foundation

struct LoginContentModel {
    var titleLogin: String = NSLocalizedString("Login", comment: "")
    var titlePassword: String = NSLocalizedString("Password", comment: "")
    var titleButton: String = NSLocalizedString("LOGIN", comment: "")
}

//
//  DashboardContent.swift
//  Architecture
//
//  Created by Lucas Carvalho on 10/06/19.
//  Copyright © 2019 Lucas Carvalho. All rights reserved.
//

import Foundation
import UIKit

/*
 Normalmente eu criaria componentes de forma mais inteligente, mas este projeto é apenas para demonstrar conhecimento.
 */

class DashboardContent: UIView {
    
    // MARK: CONSTANTS
    private struct Metrics {
        static let contentSpacing: CGFloat = 16
        static let contentTopSpacing: CGFloat = 80
    }
    
    // MARK: INITIALIZERS
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        constraintUI()
    }
    
    // MARK: UI
    private lazy var titleLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        label.textColor = UIColor.textColor
        return label
    }()
    
    // MARK: SETUP
    public func setup() {
        let date = Date()
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let formattedDate = format.string(from: date)

        titleLabel.text = formattedDate
    }
    
    private func constraintUI() {
        addSubview(titleLabel)
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: topAnchor, constant: Metrics.contentTopSpacing),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Metrics.contentSpacing),
            titleLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: Metrics.contentSpacing),
            ])
        
      
    }
}

//
//  AppCoordinator.swift
//  Architecture
//
//  Created by Lucas Carvalho on 10/06/19.
//  Copyright © 2019 Lucas Carvalho. All rights reserved.
//

import Foundation
import UIKit

class AppCoordinator : BaseCoordinator {
    
    let window : UIWindow
    
    init(window: UIWindow) {
        self.window = window
        super.init()
    }
    
    override func start() {
        let navigationController = UINavigationController()
        let myCoordinator = LoginCoordinator(navigationController: navigationController)
        
        self.store(coordinator: myCoordinator)
        myCoordinator.start()
        
        window.rootViewController = navigationController
        window.makeKeyAndVisible()
        
        myCoordinator.isCompleted = { [weak self] in
            self?.free(coordinator: myCoordinator)
        }
    }
}

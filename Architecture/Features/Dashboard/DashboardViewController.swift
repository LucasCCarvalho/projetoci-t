//
//  DashboardViewController.swift
//  Architecture
//
//  Created by Lucas Carvalho on 10/06/19.
//  Copyright © 2019 Lucas Carvalho. All rights reserved.
//

import UIKit

protocol DashboardViewControllerDelegate: class {
    func didTapLogin()
}

class DashboardViewController: UIViewController {

    // MARK: - Delegates
    weak var delegate: DashboardViewControllerDelegate?

    // MARK: - Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        constraintUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: - UI
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView(frame: .zero)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.backgroundColor = UIColor.gradientEndColor
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var dashboardContent: DashboardContent = {
        let view = DashboardContent()
        view.setup()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - SETUP
    private func setupUI() {
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.tintColor = UIColor.textColor
    }
    private func constraintUI() {
        view.addSubview(scrollView)
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
        
        view.addSubview(dashboardContent)
        
        NSLayoutConstraint.activate([
            dashboardContent.topAnchor.constraint(equalTo: scrollView.topAnchor),
            dashboardContent.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            dashboardContent.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            dashboardContent.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor)
            ])
    }
}



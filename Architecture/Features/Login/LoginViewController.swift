//
//  LoginViewController.swift
//  Architecture
//
//  Created by Lucas Carvalho on 10/06/19.
//  Copyright © 2019 Lucas Carvalho. All rights reserved.
//

import UIKit

protocol LoginViewControllerDelegate: class {
    func didTapLogin()
}

class LoginViewController: UIViewController {
    
    // MARK: - Delegates
    weak var delegate: LoginViewControllerDelegate?
    
    // MARK: - Life Cyle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        constraintUI()
    }
    
    // MARK: - UI
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView(frame: .zero)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.backgroundColor = UIColor.gradientEndColor
        scrollView.showsVerticalScrollIndicator = false
        return scrollView
    }()
    
    private lazy var loginContentView: LoginContent = {
        let view = LoginContent()
        view.setup(delegate: delegate!)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    // MARK: - SETUP
    private func setupUI() {
        navigationController?.isNavigationBarHidden = true
    }
    
    // MARK: - INITIALIZERS
    private func constraintUI() {
        view.addSubview(scrollView)
        
        NSLayoutConstraint.activate([
            scrollView.topAnchor.constraint(equalTo: view.topAnchor),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
            ])
        
        view.addSubview(loginContentView)
        
        NSLayoutConstraint.activate([
            loginContentView.topAnchor.constraint(equalTo: scrollView.topAnchor),
            loginContentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor),
            loginContentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor),
            loginContentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor)
            ])
    }
}


//
//  LoginAPI.swift
//  Architecture
//
//  Created by Lucas Carvalho on 10/06/19.
//  Copyright © 2019 Lucas Carvalho. All rights reserved.
//

import Foundation

protocol LoginAPIProtocol {
    static func login(item: LoginModel, completion: @escaping (LoginResponseModel) -> ())
}

class LoginAPI: LoginAPIProtocol {
    static func login(item: LoginModel, completion: @escaping (LoginResponseModel) -> ()) {
        let response = LoginResponseModel(allow: (item.login == "lifescan" || item.password == "lifescan") ? true : false)
        completion(response)
    }
}

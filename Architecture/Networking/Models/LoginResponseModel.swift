//
//  LoginResponseModel.swift
//  Architecture
//
//  Created by Lucas Carvalho on 10/06/19.
//  Copyright © 2019 Lucas Carvalho. All rights reserved.
//

import Foundation

struct LoginResponseModel {
    var allow: Bool
    
    init(allow: Bool) {
        self.allow = allow
    }
}

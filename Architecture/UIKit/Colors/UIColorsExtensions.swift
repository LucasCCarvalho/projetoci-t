//
//  UIColorsExtensions.swift
//  Architecture
//
//  Created by Lucas Carvalho on 10/06/19.
//  Copyright © 2019 Lucas Carvalho. All rights reserved.
//

import UIKit

fileprivate class UIColorsExtensions { }

extension UIColor {
    public class var primary: UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "primary", in: Bundle(for: UIColorsExtensions.self), compatibleWith: nil)!
        } else {
            return #colorLiteral(red: 0.8941176471, green: 0.09019607843, blue: 0.2431372549, alpha: 1)
        }
    }
    
    public class var gradientStartColor: UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "gradientStartColor", in: Bundle(for: UIColorsExtensions.self), compatibleWith: nil)!
        } else {
            return #colorLiteral(red: 0.9450980392, green: 0.9058823529, blue: 0.9058823529, alpha: 1)
        }
    }
    
    public class var gradientEndColor: UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "gradientEndColor", in: Bundle(for: UIColorsExtensions.self), compatibleWith: nil)!
        } else {
            return #colorLiteral(red: 0.7411764706, green: 0.7254901961, blue: 0.7137254902, alpha: 1)
        }
    }
    
    public class var textColor: UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "textColor", in: Bundle(for: UIColorsExtensions.self), compatibleWith: nil)!
        } else {
            return #colorLiteral(red: 0.2941176471, green: 0.3058823529, blue: 0.3607843137, alpha: 1)
        }
    }
    
    public class var buttonColor: UIColor {
        if #available(iOS 11.0, *) {
            return UIColor(named: "buttonColor", in: Bundle(for: UIColorsExtensions.self), compatibleWith: nil)!
        } else {
            return #colorLiteral(red: 0.3647058824, green: 0.737254902, blue: 0.8235294118, alpha: 1)
        }
    }
}

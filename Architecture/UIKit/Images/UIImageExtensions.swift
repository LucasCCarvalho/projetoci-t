//
//  UIImageExtensions.swift
//  Architecture
//
//  Created by Lucas Carvalho on 10/06/19.
//  Copyright © 2019 Lucas Carvalho. All rights reserved.
//

import UIKit

fileprivate class UIImageExtensions { }

extension UIImage {
    
    public class var logo: UIImage {
        return UIImage(named: "ic_logo", in: Bundle(for: UIImageExtensions.self), compatibleWith: nil)!
    }
    
    // MARK: - Default Image Function
    public class func image(named: String) -> UIImage {
        return UIImage(named: named, in: Bundle(for: UIImageExtensions.self), compatibleWith: nil) ?? UIImage()
    }
}

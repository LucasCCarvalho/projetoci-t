//
//  LoginCoordinator.swift
//  Architecture
//
//  Created by Lucas Carvalho on 10/06/19.
//  Copyright © 2019 Lucas Carvalho. All rights reserved.
//

import Foundation
import UIKit

class LoginCoordinator : BaseCoordinator {
    
    var navigationController: UINavigationController?
    
    init(navigationController :UINavigationController?) {
        self.navigationController = navigationController
    }
    
    override func start() {
        let loginViewController = LoginViewController()
        loginViewController.delegate = self
        navigationController?.pushViewController(loginViewController, animated: true)
    }
    
    func showDashboard() {
        let dashboardViewController = DashboardViewController()
        navigationController?.pushViewController(dashboardViewController, animated: true)
    }
}

// MARK: - Delegates
extension LoginCoordinator: LoginViewControllerDelegate {
    func didTapLogin() {
        showDashboard()
    }
}

extension LoginCoordinator: DashboardViewControllerDelegate {

}


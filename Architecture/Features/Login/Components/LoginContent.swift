//
//  LoginContent.swift
//  Architecture
//
//  Created by Lucas Carvalho on 10/06/19.
//  Copyright © 2019 Lucas Carvalho. All rights reserved.
//

import Foundation
import UIKit

/*
    Normalmente eu criaria componentes de forma mais inteligente, mas este projeto é apenas para demonstrar conhecimento. 
 */

class LoginContent: UIView {
    
    // MARK: CONSTANTS
    private struct Metrics {
        static let cornerRadius: CGFloat = 8
        static let textFieldSpacing: CGFloat = 4
        static let textFieldGrouSpacing: CGFloat = 24
        static let textFieldHeight: CGFloat = 40
        static let contentSpacing: CGFloat = 16
        static let heightAnchor: CGFloat = 80
        static let contentTopSpacing: CGFloat = 90
    }
    
    // MARK: - Delegates
    weak var delegate: LoginViewControllerDelegate?
    
    // MARK: INITIALIZERS
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        constraintUI()
    }
    
    // MARK: UI
    private lazy var imageView: UIImageView = {
        let imageView = UIImageView(image: nil)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()
    
    private lazy var loginLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        label.textColor = UIColor.textColor
        return label
    }()
    
    private lazy var passwordLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 1
        label.textColor = UIColor.textColor
        return label
    }()
    
    private lazy var loginTextField: UITextField = {
        let textField = UITextField(frame: .zero)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.layer.cornerRadius = Metrics.cornerRadius
        textField.layer.backgroundColor = UIColor.white.cgColor
        return textField
    }()
    
    private lazy var passwordTextField: UITextField = {
        let textField = UITextField(frame: .zero)
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.layer.cornerRadius = Metrics.cornerRadius
        textField.layer.backgroundColor = UIColor.white.cgColor
        return textField
    }()
    
    private lazy var loginButton: UIButton = {
        let button = UIButton(frame: .zero)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.layer.cornerRadius = Metrics.cornerRadius
        button.addTarget(self, action: #selector(didTapLogin), for: .touchUpInside)
        button.layer.backgroundColor = UIColor.buttonColor.cgColor
        return button
    }()
    
    // MARK: ACTIONS
    @objc private func didTapLogin() {
        let itemLogin = LoginModel(login: loginTextField.text ?? "", password: passwordTextField.text ?? "")
        
        LoginAPI.login(item: itemLogin) { [weak self] result in
            if result.allow {
                self?.delegate?.didTapLogin()
            } else {
                let mensagem = "Verifique a senha e o login e tente novamente."
                let alerta = UIAlertController(title: "Ops!", message: mensagem, preferredStyle: .alert)
                alerta.addAction(UIAlertAction(title: "Confirmar", style: .default, handler: nil))
                self?.window?.rootViewController?.present(alerta, animated: true, completion: nil)
            }
        }
    }
    
    // MARK: SETUP
    public func setup(delegate: LoginViewControllerDelegate) {
        let item = LoginContentModel()
        imageView.image = UIImage.logo
        
        loginLabel.text = item.titleLogin
        passwordLabel.text = item.titlePassword
        loginButton.setTitle(item.titleButton, for: UIControl.State.normal)
        
        self.delegate = delegate
    }
    
    private func constraintUI() {
        addSubview(imageView)
        
        NSLayoutConstraint.activate([
            imageView.topAnchor.constraint(equalTo: topAnchor, constant: Metrics.contentTopSpacing),
            imageView.heightAnchor.constraint(equalToConstant: Metrics.heightAnchor),
            imageView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Metrics.contentSpacing),
            imageView.leftAnchor.constraint(equalTo: leftAnchor, constant: Metrics.contentSpacing),
            ])
        
        addSubview(loginLabel)
        
        NSLayoutConstraint.activate([
            loginLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: Metrics.contentTopSpacing),
            loginLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Metrics.contentSpacing),
            loginLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: Metrics.contentSpacing),
            ])
        
        addSubview(loginTextField)
        
        NSLayoutConstraint.activate([
            loginTextField.topAnchor.constraint(equalTo: loginLabel.bottomAnchor, constant: Metrics.textFieldSpacing),
            loginTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Metrics.contentSpacing),
            loginTextField.leftAnchor.constraint(equalTo: leftAnchor, constant: Metrics.contentSpacing),
            loginTextField.heightAnchor.constraint(equalToConstant: Metrics.textFieldHeight)
            ])
        
        addSubview(loginLabel)
        
        NSLayoutConstraint.activate([
            loginLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: Metrics.contentTopSpacing),
            loginLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Metrics.contentSpacing),
            loginLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: Metrics.contentSpacing),
            ])
        
        addSubview(loginTextField)
        
        NSLayoutConstraint.activate([
            loginTextField.topAnchor.constraint(equalTo: loginLabel.bottomAnchor, constant: Metrics.textFieldSpacing),
            loginTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Metrics.contentSpacing),
            loginTextField.leftAnchor.constraint(equalTo: leftAnchor, constant: Metrics.contentSpacing),
            loginTextField.heightAnchor.constraint(equalToConstant: Metrics.textFieldHeight)
            ])
        
        addSubview(passwordLabel)
        
        NSLayoutConstraint.activate([
            passwordLabel.topAnchor.constraint(equalTo: loginTextField.bottomAnchor, constant: Metrics.textFieldGrouSpacing),
            passwordLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Metrics.contentSpacing),
            passwordLabel.leftAnchor.constraint(equalTo: leftAnchor, constant: Metrics.contentSpacing),
            ])
        
        addSubview(passwordTextField)
        
        NSLayoutConstraint.activate([
            passwordTextField.topAnchor.constraint(equalTo: passwordLabel.bottomAnchor, constant: Metrics.textFieldSpacing),
            passwordTextField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Metrics.contentSpacing),
            passwordTextField.leftAnchor.constraint(equalTo: leftAnchor, constant: Metrics.contentSpacing),
            passwordTextField.heightAnchor.constraint(equalToConstant: Metrics.textFieldHeight)
            ])
        
        addSubview(loginButton)
        
        NSLayoutConstraint.activate([
            loginButton.topAnchor.constraint(equalTo: passwordTextField.bottomAnchor, constant: Metrics.textFieldGrouSpacing),
            loginButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -Metrics.contentSpacing),
            loginButton.leftAnchor.constraint(equalTo: leftAnchor, constant: Metrics.contentSpacing),
            loginButton.heightAnchor.constraint(equalToConstant: Metrics.textFieldHeight)
            ])
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        endEditing(true)
    }
}
